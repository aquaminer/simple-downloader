FROM golang:alpine

WORKDIR /usr/src/app
CMD ["go", "run", "main.go"]
EXPOSE 3000
RUN apk add git && go get github.com/dgrijalva/jwt-go

COPY . /usr/src/app
RUN go build main.go