package main

import (
	"fmt"
	jwt "github.com/dgrijalva/jwt-go"
	"io"
	"strconv"
	"log"
	"net/http"
	"os"
)
var byteKey []byte
var storagePath string

func GenericRouterHandler(w http.ResponseWriter, r *http.Request) {
	tokenString := r.URL.Query().Get("token")
	if tokenString == "" {
		//Get not set, send a 400 bad request
		http.Error(w, "Get 'token' not specified in url.", 400)
		return
	}


	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(byteKey), nil
	})

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		log.Println(claims)

		var uuid  = claims["uuid"].(string)
		var headers  = claims["headers"].(map[string]interface{})


		Openfile, err := os.Open("/storage/" + uuid)
		if err != nil {
			http.Error(w, "File not found.", 404)
			return
		}
		FileHeader := make([]byte, 512)
		Openfile.Read(FileHeader)

		if headers["Content-Type"] == nil {

			headers["Content-Type"] = http.DetectContentType(FileHeader)
		}
		if headers["Content-Length"] == nil {
			FileStat, _ := Openfile.Stat()
			headers["Content-Length"] = strconv.FormatInt(FileStat.Size(), 10)
		}

		for header, val := range headers {
			w.Header().Set(header, val.(string));
		}

		Openfile.Seek(0, 0) // раніше читали 512 байт, збрасуємо
		io.Copy(w, Openfile)
	} else {
		fmt.Println(err)
	}
}

func main() {
	key, exists := os.LookupEnv("JWT_SECRET")
	if !exists {
		log.Print("err get JWT_SECRET");
		return;
	}
	byteKey = []byte(key);

	key, exists = os.LookupEnv("DOWNLOADER_PATH")
	if !exists {
		log.Print("err get DOWNLOADER_PATH");
		return;
	}

	log.Println(key)
	http.HandleFunc(key, GenericRouterHandler) // установим роутер
	err := http.ListenAndServe(":3000", nil) // задаем слушать порт
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}

}